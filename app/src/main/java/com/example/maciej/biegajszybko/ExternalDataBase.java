package com.example.maciej.biegajszybko;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.maciej.biegajszybko.R.drawable.run;

/**
 * Created by Maciej on 2017-02-21.
 */

public class ExternalDataBase extends AsyncTask<String, Void, String> {

    private ProgressDialog dialog;
    private Context context;
    private String id_user;
    private DataBaseSQLLite db;
    private RequestQueue queue;

    public ExternalDataBase(Context context, String id){
        this.context = context;
        this.id_user = id;
        this.db = new DataBaseSQLLite(this.context);
        queue = Volley.newRequestQueue(this.context);
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.setTitle("Zapisywanie zmian");
        dialog.setMessage("Proszę czekać...");
    }
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.create();
        dialog.show();

    }
    @Override
    protected String doInBackground(String... params) {
        saveChangedData(params[0]);
        return null;
    }


    private void saveChangedData(String url){
        StringRequest strReq = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{
                    Log.e("Error:", response);
                    String json = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                    JSONObject jsonObj = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                    boolean error = (boolean)jsonObj.get("error");
                    if(!error){
                        Toast.makeText(context,"Dane zostały poprawnie zapisane !" , Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        ((YourAccount)context).setVisibiliteButton();
                    }else{
                        dialog.dismiss();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(context,"Problem z JSON",Toast.LENGTH_SHORT).show();
                }
            }

        },new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("ERROR_JSON", "Sava Data Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() {

                Map<String, String>user = db.fetchUserById(id_user);
                Map<String, String> params = new HashMap<String, String>();
                params.put("email",user.get("email"));
                params.put("password",user.get("password"));
                params.put("name", user.get("name"));
                params.put("age", user.get("age"));
                params.put("weight", user.get("weight"));
                params.put("id_user", id_user);
                return params;
            }
        };
       queue.add(strReq);
    }
}
