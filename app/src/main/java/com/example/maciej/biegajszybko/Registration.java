package com.example.maciej.biegajszybko;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Registration extends AppCompatActivity {
    private  EditText inputName;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText inputPasswordRepeat;
    private RequestQueue queue;
    private ProgressDialog dialog;
    private SessionMenager session;
    private DataBaseSQLLite db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        inputName = (EditText)findViewById(R.id.name);
        inputEmail = (EditText)findViewById(R.id.email);
        inputPassword = (EditText)findViewById(R.id.password);
        inputPasswordRepeat = (EditText)findViewById(R.id.passwordRepeat);
        queue = Volley.newRequestQueue(getApplicationContext());
        dialog = new ProgressDialog(getApplicationContext());
        db = new DataBaseSQLLite(getApplicationContext());
        session = new SessionMenager(getApplicationContext());
    }

    public void RegisterUser(View view){
        String name = inputName.getText().toString();
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        String passwordRepeat = inputPasswordRepeat.getText().toString();

        if(!name.isEmpty() && !email.isEmpty() && !password.isEmpty() && !passwordRepeat.isEmpty()){
                if(password.equals(passwordRepeat)){
                    loadRegisterData();
                }else{
                    inputPassword.setText("");
                    inputPasswordRepeat.setText("");
                    Toast.makeText(getApplicationContext(),"Podane hasła nie są identyczne!", Toast.LENGTH_SHORT).show();
                }
        }else{
            Toast.makeText(getApplicationContext(),"Uzypełnij wszystkie pola !", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadRegisterData(){

            new LoadDataJSONFile().execute(ApiLoginConfig.URL_REGISTER);
    }

        class LoadDataJSONFile extends AsyncTask<String, Void, String>{

            protected void onPreExecute() {
                super.onPreExecute();
                dialog = new ProgressDialog(Registration.this);
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setCancelable(true);
                dialog.setTitle("Rejestracja użytkownika");
                dialog.setMessage("Proszę czekać...");
                dialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                AddRegisterUser(params[0]);
                return null;
            }
        }

    private void AddRegisterUser(String url){
        StringRequest strReq = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try{
                    JSONObject jsonObj = new JSONObject(response);
                    boolean error = (boolean)jsonObj.get("error");
                    if(!error){
                        String name = (String)jsonObj.get("name");
                        String email = (String)jsonObj.get("email");
                        String pass = (String)jsonObj.get("password");
                        String age = "";
                        String weight = "";
                        User user = new User(name, pass,email, age, weight);
                        db.AddUser(user);
                        session.setLoggedIn(true);
                        session.addPreferences("USER_NAME", user.getName());
                        Intent intent = new Intent(Registration.this, UserActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Użytkownik został utworzony poprawnie" , Toast.LENGTH_SHORT);
                        finish();
                    }else{
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(),jsonObj.get("error_msg").toString(),Toast.LENGTH_SHORT).show();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Problem z JSON",Toast.LENGTH_SHORT);
                }
            }

        },new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("ERROR_JSON", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {

                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();
                String name = inputName.getText().toString();
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                params.put("name", name);

                return params;
            }
        };
        queue.add(strReq);
    }
}
