package com.example.maciej.biegajszybko;

import android.content.Context;
import android.provider.Settings;

/**
 * Created by Maciej on 2016-12-08.
 */

public class Chronometer implements Runnable {

    public static final long MILIS_TO_MINUTES = 60000;
    public static final long MILIS_TO_HOURS = 3600000;

    private Context mContext;
    private long mStartTime;
    private long startPause;
    private long finishPause;
    private long lenghtPause;
    private boolean isRunning;
    private boolean pause;


    public Chronometer(Context context){
        mContext = context;

    }

    public void start(){
        if(pause){
            pause = false;
            isRunning = true;
            finishPause = System.currentTimeMillis();
            lenghtPause += finishPause - startPause;
            synchronized (this){
                notify();
            }
        }else {
            mStartTime = System.currentTimeMillis();
            isRunning = true;
            startPause = 0;
            finishPause = 0;
            lenghtPause = 0;
        }
    }

    public void stop(){
        isRunning = false;

    }

    public void pause(){
        pause = true;
        isRunning = false;
        startPause = System.currentTimeMillis();

    }

    @Override
    public void run() {
        while(isRunning){
            long since = System.currentTimeMillis() - mStartTime - lenghtPause;
            int seconds = (int) ((since/1000) % 60);
            int minutes = (int) ((since /MILIS_TO_MINUTES)%60);
            int hours = (int)((since / MILIS_TO_HOURS)%60);
            int milisec =  (int) since % 1000;
            ((UserActivity)mContext).updateTimerText(String.format("%02d:%02d:%02d:%03d",hours,minutes,seconds,milisec));

            try {
                Thread.sleep(10);
                synchronized (this){
                    while(pause){
                        wait();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
