package com.example.maciej.biegajszybko;

import android.content.Context;
import android.support.test.espresso.core.deps.guava.io.ByteArrayDataInput;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maciej on 2017-03-02.
 */

public class TrackAdapterList extends ArrayAdapter<StatisticUser> {

    private final Context context;
    private  List<StatisticUser> values;
    private List<LatLng>points_track;

    public TrackAdapterList(Context contex, List<StatisticUser> values){
        super(contex, R.layout.account_list,values);
        this.context = contex;
        this.values = values;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.track_list, parent, false);
        TextView time = (TextView) rowView.findViewById(R.id.time);
        TextView date = (TextView) rowView.findViewById(R.id.date);
        TextView distance = (TextView) rowView.findViewById(R.id.distance);
        Button showTrackButton = (Button) rowView.findViewById(R.id.buttonShowTrack);
        final StatisticUser statisticUser = getItem(position);
        time.setText(statisticUser.getTime());
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm ");
        String tmp = formatter.format(statisticUser.getDate());
        date.setText(tmp);
        distance.setText(statisticUser.getDistance());
        showTrackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                points_track = new ArrayList<LatLng>();
                byte[]arrayByteFile=statisticUser.getArray();
                try {
                    File file = new  File(context.getFilesDir()+"/tmp.txt");
                    if(file.exists()){

                    }else{
                        file.createNewFile();
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(arrayByteFile);
                    fileOutputStream.close();
                    FileInputStream in = new FileInputStream(new File(context.getFilesDir()+"/tmp.txt"));
                    BufferedInputStream buffor = new BufferedInputStream(in);
                    StringBuffer sb = new StringBuffer();
                    while(buffor.available()!=0){
                        char c = (char)buffor.read();
                        sb.append(c);
                    }
                    int j = 0;
                    String[]tabTmp = sb.toString().split("\\-");
                    if(tabTmp.length > 1) {
                        for (int i = 0; i < tabTmp.length; ++i) {
                            String[] tmp = tabTmp[i].split("\\,");
                            LatLng one_point_track = new LatLng(Double.valueOf(tmp[j]), Double.valueOf(tmp[j + 1]));
                            points_track.add(one_point_track);
                            j = 0;
                        }
                    }
                    ((MapsActivity) context).drawTrackOnTheMap();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        return rowView;
    }

    public List<LatLng>getPoints_track(){
        return points_track;
    }
}
