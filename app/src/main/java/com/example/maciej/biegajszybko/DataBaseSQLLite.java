package com.example.maciej.biegajszybko;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.support.test.espresso.core.deps.guava.io.ByteArrayDataOutput;
import android.support.test.espresso.core.deps.guava.io.Files;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;
import static com.example.maciej.biegajszybko.R.menu.user;


/**
 * Created by Maciej on 2016-11-22.
 */

public class DataBaseSQLLite extends SQLiteOpenHelper {

    private SQLiteDatabase db;
    private static final String TABLE_NAME_USER = "User";
    private static final String TABLE_NAME_STATISTIC = "Statistic";
    private static final String TABLE_NAME_LOCATIONS="Locations";
    private static final String DataBaseName = "LocalDataBase";
    private static final String CREATE_TABLE_USER="CREATE TABLE "+TABLE_NAME_USER+ "(id_user INTEGER PRIMARY KEY,name TEXT, password TEXT, email TEXT, age TEXT DEFAULT NULL, weight TEXT DEFAULT NULL)";
    private static final String CREATE_TABLE_STATISTIC=" CREATE TABLE IF NOT EXISTS " + TABLE_NAME_STATISTIC + "(id_statistic INTEGER PRIMARY KEY, Time TEXT, distance TEXT, date TEXT, track BLOB)";

    public  DataBaseSQLLite(Context context){
        super(context, DataBaseName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_STATISTIC);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS User");
        db.execSQL("DROP TABLE IF EXISTS Locations");
        db.execSQL("DROP TABLE IF EXISTS Statistic");
        onCreate(db);
    }

    public void AddUser(User user){
        ContentValues values = new ContentValues();
        values.put("name", user.getName());
        values.put("password",user.getPassword());
        values.put("email", user.getEmail());
        values.put("age", user.getAge());
        values.put("weight", user.getWeight());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME_USER,null, values);
        db.close();
    }

    public Map<String, String> fetchUserByEmail(String email){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM User WHERE email=?", new String[]{email});
        cursor.moveToFirst();
        String[]columnNames=cursor.getColumnNames();
        Map<String, String> map = new HashMap<String, String>();
        for (int i =0; i<columnNames.length;++i){
            map.put(columnNames[i],cursor.getString(cursor.getColumnIndexOrThrow(columnNames[i])));
        }
        return map;
    }

    public void updateDataUser(String nameColumn, String value, String id_User){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "UPDATE User SET "+nameColumn+"=\'"+value+"\' WHERE id_user="+id_User+";";
        db.execSQL(sql);
        db.close();
    }

    public Map<String, String> fetchUserById(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM User WHERE id_user=?", new String[]{id});
        cursor.moveToFirst();
        String[]columnNames=cursor.getColumnNames();
        Map<String, String> map = new HashMap<String, String>();
        for (int i =0; i<columnNames.length;++i){
            map.put(columnNames[i],cursor.getString(cursor.getColumnIndexOrThrow(columnNames[i])));
        }
        return map;
    }

    public void insertLocationPoint(String latitude, String longtitude){
        ContentValues values = new ContentValues();
        values.put("lat", latitude);
        values.put("long",longtitude);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME_LOCATIONS,null, values);
    }

    public void insertStatistic(String time, String distance, Date date, File file) throws IOException {
        ContentValues values = new ContentValues();
        byte[] arrayByteFile = convertFileToByteArray(file);

        values.put("Time",time);
        values.put("distance", distance);
        values.put("date",date.toString());
        values.put("track",arrayByteFile);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME_STATISTIC,null, values);
        db.close();

    }

    public List<StatisticUser> getAllStatisticUser(){
        List<StatisticUser>arrayStatisticUser = new ArrayList<StatisticUser>();
        String sql = "SELECT * FROM "+TABLE_NAME_STATISTIC;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do {
                StatisticUser statisticUser = new StatisticUser();
                Date date = new Date(cursor.getString(cursor.getColumnIndex("date")));
                statisticUser.setDate(date);
                statisticUser.setTime(cursor.getString(cursor.getColumnIndex("Time")));
                statisticUser.setDistance(cursor.getString(cursor.getColumnIndex("distance")));
                byte[] array = cursor.getBlob(cursor.getColumnIndex("track"));
                statisticUser.setArray(array);
                arrayStatisticUser.add(statisticUser);
            }while(cursor.moveToNext());
        }
        db.close();
        return arrayStatisticUser;


    }

    private byte[] convertFileToByteArray(File file) throws IOException {
        return Files.toByteArray(file);
    }


}
