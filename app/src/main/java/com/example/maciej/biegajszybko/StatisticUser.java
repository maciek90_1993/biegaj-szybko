package com.example.maciej.biegajszybko;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Maciej on 2017-03-03.
 */

public class StatisticUser {
    private Date date;
    private String time;
    private String distance;
    private ArrayList<LatLng> points_track;
    private File file;
    private byte[]array;


    public byte[] getArray() {return array;}

    public void setArray(byte[] array) {this.array = array;}

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public ArrayList<LatLng> getPoints_track() {
        return points_track;
    }

    public void setPoints_track(ArrayList<LatLng> points_track) {
        this.points_track = points_track;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
