package com.example.maciej.biegajszybko;

/**
 * Created by Maciej on 2016-11-15.
 */

public class ApiLoginConfig {
    public static String URL_LOGIN = "http://www.biegajszybko.cba.pl/BiegajSzybko/login.php";
    public static String URL_REGISTER = "http://www.biegajszybko.cba.pl/BiegajSzybko/register.php";
    public static String URL_UPDATA = "http://www.biegajszybko.cba.pl/BiegajSzybko/update.php";
    public static String URL_SAVE_STATISTIC = "http://www.biegajszybko.cba.pl/BiegajSzybko/statistic.php";
}
