package com.example.maciej.biegajszybko;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.test.espresso.core.deps.guava.collect.MapMaker;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.location.LocationListener;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private LocationManager locationMenager;
    private Location myCurrentLocation;
    private String theBestProvider;
    private Location myLastKknowLocation;
    private Boolean GPS_status;
    private Button button_start, button_stop;
    private Criteria criteria;
    private SessionMenager sesssion;
    private List<LatLng> points_path;
    private AppCompatDelegate delegate;
    private ListView tract_list;
    private TrackAdapterList adapter;
    private StatisticUserAdapterList adapterList;
    private Button showTrackButton;
    private DataBaseSQLLite db;
    private List<StatisticUser>arrayStatisticUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        toolbar.setTitle("Trasy");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_track);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sesssion = new SessionMenager(getApplicationContext());
        db = new DataBaseSQLLite(getApplicationContext());
        arrayStatisticUser = db.getAllStatisticUser();
        tract_list = (ListView)findViewById(R.id.tract_list);
        String[] tab = {"0:00:000-04 02 2017-12km","0:10:120-10 02 2017-13km","0:10:120-10 02 2017-13km","0:16:120-10 02 2017-10km" };
        adapter = new TrackAdapterList(MapsActivity.this, arrayStatisticUser);
        tract_list.setClickable(true);
        tract_list.setAdapter(adapter);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user, menu);
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_track);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            this.LogOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.userBox) {
            Intent intent = new Intent(MapsActivity.this, YourAccount.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.tracks) {
            Intent intent = new Intent(MapsActivity.this, MapsActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.statistic) {
            Intent intent = new Intent(MapsActivity.this, Statistic.class);
            startActivity(intent);
            finish();
        } else if(id == R.id.traning){
            Intent intent = new Intent(MapsActivity.this, UserActivity.class);
            startActivity(intent);
        }else if(id == R.id.exit){
            AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
            builder.setMessage(R.string.info_exit);
            builder.setCancelable(true);
            builder.setPositiveButton(R.string.info_exit_yes, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MapsActivity.this.finish();
                }
            });
            builder.setNegativeButton(R.string.info_exit_no, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_track);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void LogOut(){
        boolean login = sesssion.isLoggedIn();
        if(login){
            sesssion.setLoggedIn(false);
            sesssion.deleteAllPreferences();
            Intent intent = new Intent(getApplicationContext(), SingIn.class);
            startActivity(intent);
            finish();
            Toast.makeText(getApplicationContext(),"Zostałeś poprawnie wylogowany",Toast.LENGTH_SHORT).show();
        }else{
            finish();
            Toast.makeText(getApplicationContext(),"Problem z wylogowaniem",Toast.LENGTH_SHORT).show();
        }
    }

    public void drawTrackOnTheMap(){
        mMap.clear();
        points_path = adapter.getPoints_track();
        if(!points_path.isEmpty()) {
            PolylineOptions options = new PolylineOptions().addAll(points_path).width(10).color(Color.BLUE).geodesic(true);
            mMap.addPolyline(options);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(points_path.get(0), 15));
            mMap.addMarker(new MarkerOptions().position(points_path.get(0)).title("START"));
            mMap.addMarker(new MarkerOptions().position(points_path.get(points_path.size()-1)).title("KONIEC"));
        }else{
            Toast.makeText(MapsActivity.this, "Trasa nie została zarejestrowana podczas tego treningu.",Toast.LENGTH_LONG).show();
        }

    }

}
