package com.example.maciej.biegajszybko;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import static android.content.Context.LOCATION_SERVICE;


/**
 * Created by Maciej on 2016-12-16.
 */

public class LocationService implements LocationListener {


    private Context mContext;
    private LocationManager locationMenager;
    private Location myCurrentLocation;
    private String theBestProvider;
    private Location myLastKknowLocation;
    private Boolean GPS_status;
    private boolean locationChange;
    private boolean isRunnig;
    private Criteria criteria;
    private List<LatLng> points_path;
    private float distance;
    private UserActivity userActivity;

    public LocationService(Context context) {
        userActivity = new UserActivity();

        isRunnig = true;
        locationChange = false;
        mContext = context;
        distance = 0;
        locationMenager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        GPS_status = locationMenager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (GPS_status) {
            criteria = new Criteria();
            theBestProvider = locationMenager.getBestProvider(criteria, true);
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            locationMenager.requestLocationUpdates(theBestProvider, 1000,10 , this);
            myLastKknowLocation = locationMenager.getLastKnownLocation(theBestProvider);
        }
    }




    @Override
    public void onLocationChanged(Location location) {
        if(myLastKknowLocation != null) {
            distance += location.distanceTo(myLastKknowLocation) / 1000;
            reload(location);
            Log.d("DYSTANS: ", String.valueOf(distance));
        }else{
            myLastKknowLocation = location;
        }
        Log.d("PUNKT LOKALIZACJI: ", location.toString());


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(mContext,"Gps złapał sygnał.",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(mContext,"Gps stracił sygnał.",Toast.LENGTH_SHORT).show();

    }


    private void reload(Location location) throws SecurityException {
        myCurrentLocation = location;
        theBestProvider = locationMenager.getBestProvider(criteria, true);
        myLastKknowLocation = locationMenager.getLastKnownLocation(theBestProvider);

    }



    public double getDistance(){
        return  (double) distance;
    }




}
