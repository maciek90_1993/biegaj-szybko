package com.example.maciej.biegajszybko;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by Maciej on 2017-02-16.
 */

public class AccountAdapterList extends ArrayAdapter<String> {

    private final Context context;
    private  String[] values;

    public AccountAdapterList(Context contex,String[] values){
        super(contex, R.layout.account_list,values);
        this.context = contex;
        this.values = values;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.account_list, parent, false);
        TextView key = (TextView) rowView.findViewById(R.id.key);
        TextView value = (TextView) rowView.findViewById(R.id.value);
        String[] tmp = values[position].split("\\-");
        key.setText(tmp[0]);
        value.setText(tmp[1]);

        return rowView;
    }

    public String[] getData(){
        return this.values;
    }

    public void setData(String[] tmp){
        this.values = tmp;
    }

}
