package com.example.maciej.biegajszybko;

/**
 * Created by Maciej on 2016-11-22.
 */

public class User {


    private String password;
    private String email;
    private String name;
    private String age;
    private String weight;


    public User ( String name, String password, String email, String age, String weight){
        this.name = name;
        this.password = password;
        this.email = email;
        this.age = age;
        this.weight = weight;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
