package com.example.maciej.biegajszybko;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

public class YourAccount extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private SessionMenager sesssion;
    private ListView listView_account;
    private DataBaseSQLLite db;
    private TextView name_TV_headerNav1;
    private TextView email_TV_headerNav1;
    private Button save_data;
    private AccountAdapterList adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_account);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_account);
        toolbar.setTitle("Twoje Konto");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_account);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_account);
        View hView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        
        db = new DataBaseSQLLite(getApplicationContext());
        save_data = (Button)findViewById(R.id.save_data);
        name_TV_headerNav1 = (TextView) hView.findViewById(R.id.nameNav);
        email_TV_headerNav1 = (TextView) hView.findViewById(R.id.emailNav);
        String[] tmp = new String[4];
        sesssion = new SessionMenager(getApplicationContext());
        name_TV_headerNav1.setText(sesssion.getPreference("USER_NAME"));
        email_TV_headerNav1.setText(sesssion.getPreference("USER_EMAIL"));

            save_data.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new ExternalDataBase(YourAccount.this,sesssion.getPreference("ID_USER")).execute(ApiLoginConfig.URL_UPDATA);
                }
            });

         final Map<String, String> map = db.fetchUserByEmail(sesssion.getPreference("USER_EMAIL"));
        int i =0;
        for (Map.Entry<String, String> items : map.entrySet()) {
            if(items.getKey().equals("id_user") || items.getKey().equals("password")){

            }else {
                tmp[i] = items.getKey() + "-" + items.getValue();
                i++;
            }
        }
        listView_account = (ListView) findViewById(R.id.listView_account);
        adapter = new AccountAdapterList(YourAccount.this, tmp);
        listView_account.setAdapter(adapter);
        listView_account.setClickable(true);
        listView_account.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(YourAccount.this);
                LayoutInflater inflater = getLayoutInflater();
                final View convertView = (View) inflater.inflate(R.layout.form, null);
                //EDYCJA Email
                if(position == 0) {
                   alertDialog.setView(convertView);
                   alertDialog.setTitle(R.string.title_edit_email);
                   alertDialog.setPositiveButton("Zmień", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           EditText edit_data_alert_dialog = (EditText) convertView.findViewById(R.id.edit_name_alert_dialog);
                           String textData = edit_data_alert_dialog.getText().toString();
                           if(textData.isEmpty()) {
                               Toast.makeText(YourAccount.this, "Nie uzupełniono pola !!!", Toast.LENGTH_SHORT).show();

                           }else{
                               final String id_user = map.get("id_user");
                               sesssion.addPreferences("USER_EMAIL",textData);
                               db.updateDataUser("email",textData, id_user);
                               Toast.makeText(YourAccount.this, "Dane zostały zmienione", Toast.LENGTH_SHORT).show();
                               reloadDataList();
                               save_data.setVisibility(View.VISIBLE);
                           }
                       }
                   });
                   alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           dialog.cancel();
                       }
                   });
                   AlertDialog alert = alertDialog.create();
                   alert.show();
               }
                //EDYCJA Imienia
                if(position == 1) {
                    alertDialog.setView(convertView);
                    alertDialog.setTitle(R.string.title_edit_name);
                    alertDialog.setPositiveButton("Zmień", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText edit_data_alert_dialog = (EditText) convertView.findViewById(R.id.edit_name_alert_dialog);
                            String textData = edit_data_alert_dialog.getText().toString();
                            if(textData.isEmpty()) {
                                Toast.makeText(YourAccount.this, "Nie uzupełniono pola !!!", Toast.LENGTH_SHORT).show();

                            }else{
                                sesssion.addPreferences("USER_NAME",textData);
                                final String id_user = map.get("id_user");
                                db.updateDataUser("name",textData, id_user);
                                save_data.setVisibility(View.VISIBLE);
                                reloadDataList();
                                Toast.makeText(YourAccount.this, "Dane zostały zmienione ", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                    alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = alertDialog.create();
                    alert.show();
                }
                //EDYCJA WIEKU
                if(position==2){
                    alertDialog.setView(convertView);
                    alertDialog.setTitle(R.string.title_edit_age);
                    alertDialog.setPositiveButton("Zmień", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText edit_data_alert_dialog = (EditText) convertView.findViewById(R.id.edit_name_alert_dialog);
                            String textData = edit_data_alert_dialog.getText().toString();
                            if(textData.isEmpty()) {
                                Toast.makeText(YourAccount.this, "Nie uzupełniono pola !!!", Toast.LENGTH_SHORT).show();
                            }else{
                                final String id_user = map.get("id_user");
                                db.updateDataUser("age",textData, id_user);
                                save_data.setVisibility(View.VISIBLE);
                                reloadDataList();
                                Toast.makeText(YourAccount.this, "Dane zostały zmienione", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = alertDialog.create();
                    alert.show();
                }
                //EDYCJA WAGI
                if(position == 3){
                    alertDialog.setView(convertView);
                    alertDialog.setTitle(R.string.title_edit_weight);
                    alertDialog.setPositiveButton("Zapisz", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText edit_data_alert_dialog = (EditText) convertView.findViewById(R.id.edit_name_alert_dialog);
                            String textData = edit_data_alert_dialog.getText().toString();
                            if(textData.isEmpty()) {
                                Toast.makeText(YourAccount.this, "Nie uzupełniono pola !!!", Toast.LENGTH_SHORT).show();

                            }else{
                                final String id_user = map.get("id_user");
                                db.updateDataUser("weight",textData, id_user);
                                save_data.setVisibility(View.VISIBLE);
                                reloadDataList();
                                Toast.makeText(YourAccount.this, "Dane zostały zmienione", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = alertDialog.create();
                    alert.show();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_account);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            this.LogOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void LogOut(){
        boolean login = sesssion.isLoggedIn();
        if(login){
            sesssion.setLoggedIn(false);
            sesssion.deleteAllPreferences();
            Intent intent = new Intent(getApplicationContext(), SingIn.class);
            startActivity(intent);
            finish();
            Toast.makeText(getApplicationContext(),"Zostałeś poprawnie wylogowany",Toast.LENGTH_SHORT).show();
        }else{
            finish();
            Toast.makeText(getApplicationContext(),"Problem z wylogowaniem",Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.userBox) {
            Intent intent = new Intent(YourAccount.this, YourAccount.class );
            startActivity(intent);
            finish();
        } else if (id == R.id.tracks) {
            Intent intent = new Intent(YourAccount.this, MapsActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.statistic) {

        } else if(id == R.id.traning){
            Intent intent = new Intent(YourAccount.this, UserActivity.class);
            startActivity(intent);
             finish();
        } else if(id == R.id.exit){
            AlertDialog.Builder builder = new AlertDialog.Builder(YourAccount.this);
            builder.setMessage(R.string.info_exit);
            builder.setCancelable(true);
            builder.setPositiveButton(R.string.info_exit_yes, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    YourAccount.this.finish();

                }
            });

            builder.setNegativeButton(R.string.info_exit_no, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_account);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void reloadDataList(){

        final Map<String, String> map = db.fetchUserByEmail(sesssion.getPreference("USER_EMAIL"));
        String[] tmp = new String[4];
        int i =0;
        for (Map.Entry<String, String> items : map.entrySet()) {
            if(items.getKey().equals("id_user") || items.getKey().equals("password")){

            }else {
                tmp[i] = items.getKey() + "-" + items.getValue();
                i++;

            }
        }
        adapter.setData(tmp);
        adapter.notifyDataSetChanged();
        name_TV_headerNav1.setText(sesssion.getPreference("USER_NAME"));
        email_TV_headerNav1.setText(sesssion.getPreference("USER_EMAIL"));
    }
    public void setVisibiliteButton(){
        save_data.setVisibility(View.INVISIBLE);
    }

}
