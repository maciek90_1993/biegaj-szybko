package com.example.maciej.biegajszybko;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.audiofx.BassBoost;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Maciej on 2016-12-18.
 */
public class GPS_Service extends Service {

    private LocationListener listener;
    private LocationManager locationManager;
    private Criteria criteria;
    private String theBestProvider;
    private Location theLastKnowMyLocation;
    private float distance;
    private PowerManager mgr;
    private PowerManager.WakeLock wakeLock;
    private List<LatLng> points_track;
    private DataBaseSQLLite db;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        distance = 0;
        db = new DataBaseSQLLite(getApplicationContext());
        points_track = new ArrayList<LatLng>();
        mgr = (PowerManager)getApplicationContext().getSystemService(Context.POWER_SERVICE);
        wakeLock= mgr.newWakeLock(PowerManager.FULL_WAKE_LOCK, "MyWakeLock");
        wakeLock.acquire();
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(theLastKnowMyLocation != null){
                    distance += location.distanceTo(theLastKnowMyLocation) / 1000;
                    points_track.add(new LatLng(location.getLatitude(),location.getLongitude()));
                }
                theLastKnowMyLocation = location;
                Intent i = new Intent("location_update");
                i.putExtra("coordinates", location.getLongitude()+" "+location.getLatitude());
                i.putExtra("Distance",distance);
                i.putExtra("DistanceInMeters", distance*1000);
                sendBroadcast(i);
                Log.d("GPS_SERVICE_DISTANCE:",String.valueOf(distance));
                Log.d("GPS_Location",location.toString());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, listener);
        criteria = new Criteria();
        theBestProvider = locationManager.getBestProvider(criteria, true);
        //noinspection MissingPermission
        theLastKnowMyLocation = locationManager.getLastKnownLocation(theBestProvider);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return  Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        wakeLock.release();
        if(locationManager != null){
            //noinspection MissingPermission
            locationManager.removeUpdates(listener);
        }
    }
    public List<LatLng>getPoints_track(){
        return this.points_track;
    }
}
