package com.example.maciej.biegajszybko;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class Statistic extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private SessionMenager sesssion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_statistic);
        toolbar.setTitle("Statystyki");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_statistic);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_stattistic);
        View hView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            this.LogOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.userBox) {
            Intent intent = new Intent(Statistic.this, YourAccount.class );
            startActivity(intent);
            finish();
        } else if (id == R.id.tracks) {
            Intent intent = new Intent(Statistic.this, MapsActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.statistic) {
            Intent intent = new Intent(Statistic.this, Statistic.class);
            startActivity(intent);
            finish();
        } else if(id == R.id.traning){
            Intent intent = new Intent(Statistic.this, UserActivity.class);
            startActivity(intent);
            finish();
        } else if(id == R.id.exit){
            AlertDialog.Builder builder = new AlertDialog.Builder(Statistic.this);
            builder.setMessage(R.string.info_exit);
            builder.setCancelable(true);
            builder.setPositiveButton(R.string.info_exit_yes, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Statistic.this.finish();

                }
            });

            builder.setNegativeButton(R.string.info_exit_no, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_statistic);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void LogOut(){
        boolean login = sesssion.isLoggedIn();
        if(login){
            sesssion.setLoggedIn(false);
            sesssion.deleteAllPreferences();
            Intent intent = new Intent(getApplicationContext(), SingIn.class);
            startActivity(intent);
            finish();
            Toast.makeText(getApplicationContext(),"Zostałeś poprawnie wylogowany",Toast.LENGTH_SHORT).show();
        }else{
            finish();
            Toast.makeText(getApplicationContext(),"Problem z wylogowaniem",Toast.LENGTH_SHORT).show();
        }
    }
}
