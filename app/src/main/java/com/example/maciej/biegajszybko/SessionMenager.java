package com.example.maciej.biegajszybko;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Maciej on 2016-11-19.
 */

public class SessionMenager {
    Context _context;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private static final String PREFERENCE_NAME = "LOGIN_PREF";
    private static final String KEY_LOGGED_IN = "isLoggedIn";


    public SessionMenager(Context context){
        this._context = context;
        this.preferences = this._context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        this.editor = this.preferences.edit();
    }

    public void setLoggedIn(Boolean isLoggedIn){
        this.editor.putBoolean(KEY_LOGGED_IN, isLoggedIn);
        this.editor.commit();
    }

    public boolean isLoggedIn(){
        return preferences.getBoolean(KEY_LOGGED_IN, false);
    }

    public void addPreferences(String key, String value){
        this.editor.putString(key, value);
        this.editor.commit();
    }
    public String getPreference(String key){
       return this.preferences.getString(key,"");

    }

    public void deleteAllPreferences(){
        this.editor.clear().commit();
    }
}
