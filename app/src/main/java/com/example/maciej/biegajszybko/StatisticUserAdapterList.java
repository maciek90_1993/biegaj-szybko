package com.example.maciej.biegajszybko;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Maciej on 2017-03-11.
 */

public class StatisticUserAdapterList extends BaseExpandableListAdapter{

    private List<StatisticUser> data;
    private Context context;

    StatisticUserAdapterList(Context context, List<StatisticUser> data){
        this.context = context;
        this.data = data;
    }


    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.track_list, parent, false);
        TextView time = (TextView) rowView.findViewById(R.id.time);
        TextView date = (TextView) rowView.findViewById(R.id.date);
        TextView distance = (TextView) rowView.findViewById(R.id.distance);
        Button showTrackButton = (Button) rowView.findViewById(R.id.buttonShowTrack);
        time.setText(data.get(groupPosition).getTime());
        date.setText(data.get(groupPosition).getDate().toString());
        distance.setText(data.get(groupPosition).getDistance());
        return rowView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
