package com.example.maciej.biegajszybko;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Maciej on 2017-03-03.
 */

public class ExternalDatabaseStatisticUser extends AsyncTask<String, Void, String> {

    private ProgressDialog dialog;
    private Context context;
    private String id_user;
    private RequestQueue queue;
    private StatisticUser statisticUser;
    private ArrayList<LatLng> points_track;
    private String pointTrack;
    ExternalDatabaseStatisticUser(Context context, String id_user, StatisticUser statisticUser){
        this.context = context;
        this.id_user = id_user;
        this.statisticUser = statisticUser;

        pointTrack ="";
        queue = Volley.newRequestQueue(this.context);
        points_track = new ArrayList<LatLng>();
        points_track = statisticUser.getPoints_track();


        dialog = new ProgressDialog(this.context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.setTitle("Zapisywanie danych");
        dialog.setMessage("Proszę czekać...");
    }

    protected void onPreExecute() {
        super.onPreExecute();
        dialog.create();
          dialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        saveStatisticData(params[0]);
        return null;
    }

    private void saveStatisticData(String url){
        StringRequest strReq = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{
                    Log.e("Error:", response);
                    String json = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1);
                    JSONObject jsonObj = new JSONObject(response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1));
                    boolean error = (boolean)jsonObj.get("error");
                    if(!error){
                        Toast.makeText(context,"Dane zostały poprawnie zapisane !"+jsonObj.get("id_statistic"), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }else{
                        dialog.dismiss();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(context,"Problem z JSON",Toast.LENGTH_SHORT).show();
                }
            }

        },new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("ERROR_JSON", "Sava Data Error: " + error.getMessage());
                Toast.makeText(context,
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("time",statisticUser.getTime());
                params.put("date",statisticUser.getDate().toString());
                params.put("distance", statisticUser.getDistance());
                params.put("id_user", id_user);
                return params;
            }
        };
        queue.add(strReq);
    }
}
