package com.example.maciej.biegajszybko;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private TextView textView4;
    private String stringURL;
    private String stringURL1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView4 = (TextView) findViewById(R.id.textView4);
        stringURL = "http://strefakursow.pl";
        stringURL1 ="http://192.168.1.102/BiegajSzybciej/index.php";
    }

    public void loadMap(View view) {
        Intent intent = new Intent(MainActivity.this, MapsActivity.class);
        startActivity(intent);
    }

    public void checkConectingNetwork(View view) {
        TextView textView2 = (TextView) findViewById(R.id.textView2);
        ConnectivityManager conMenager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infoNet = conMenager.getActiveNetworkInfo();
        if (infoNet != null && infoNet.isConnected()) {
            textView2.setText("Sieć działa prawidłowo");
        } else {
            textView2.setText("Brak połączenia");
        }
    }



    public void downloadJSON(View view){
        new GetJSONFile().execute(stringURL1);
    }

    class GetJSONFile extends AsyncTask<String, Void, String>{
         InputStream inputStream = null;
         int returnedCodeServer;
        HttpURLConnection conn;
        @Override
        protected String doInBackground(String... params) {
            try {
                URL url =new URL(params[0]);
                 conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                returnedCodeServer = conn.getResponseCode();
                switch (returnedCodeServer){
                    case 200: Log.e("ERROR", "Error"+returnedCodeServer);
                    case 201:{
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line;
                        while((line = br.readLine()) != null){
                            stringBuilder.append(line + "\n");
                        }
                        br.close();
                        return stringBuilder.toString();
                    }

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }

        protected void onPostExecute(String result){
            textView4.setText("LocalHost:"+result);
            conn.disconnect();
        }

    }
}

