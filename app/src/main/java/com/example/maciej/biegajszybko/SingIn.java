package com.example.maciej.biegajszybko;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.android.volley.Response;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SingIn extends AppCompatActivity {

    private EditText passwordInput;
    private EditText emailInput;
    private RequestQueue queue;
    private SessionMenager sesssion;
    private DataBaseSQLLite db;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);
        passwordInput = (EditText) findViewById(R.id.password);
        emailInput = (EditText) findViewById(R.id.email);
        queue = Volley.newRequestQueue(SingIn.this);
        db = new DataBaseSQLLite(getApplicationContext());
        sesssion = new SessionMenager(getApplicationContext());
        dialog = new ProgressDialog(getApplicationContext());

        if(sesssion.isLoggedIn()){
            Intent intent = new Intent(SingIn.this, UserActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void loadRegistrationActivity(View view){
        Intent intent = new Intent(SingIn.this, Registration.class);
        startActivity(intent);
    }

    public void LogInUser(View view){
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();

        if(!email.isEmpty() && !password.isEmpty()){
            getUserFromDataBase();
        }else{
            Toast info = Toast.makeText(SingIn.this,"Wprowadź email i hasło !!!",
                    Toast.LENGTH_SHORT );
            info.show();
        }
    }

    private void getUserFromDataBase(){
        new GetDataJSONFile().execute(ApiLoginConfig.URL_LOGIN);
    }


    class GetDataJSONFile extends AsyncTask<String, Void, String>{

        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(SingIn.this);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(true);
            dialog.setTitle("Logowanie");
            dialog.setMessage("Proszę czekać...");
            dialog.show();
        }

            @Override
            protected String doInBackground(String... params) {
                getJSON(params[0]); // wywołanie metody, która pobierze JSON-a, pobiera parametr link do skryptu php.
                return null;
            }
        }

    private void getJSON(String url){
        StringRequest strReq = new StringRequest(Request.Method.POST,url,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObj = new JSONObject(response);
                    boolean error = (boolean)jsonObj.get("error");
                    if(!error){
                        String name = (String)jsonObj.get("name");
                        String email = (String)jsonObj.get("email");
                        String pass = (String)jsonObj.get("password");
                        String age = (String)jsonObj.get("age");
                        String weight = (String)jsonObj.get("weight");
                        String inputEmail =  emailInput.getText().toString();
                        String inputPass = passwordInput.getText().toString();
                        if(inputEmail.equals(email) && inputPass.equals(pass)){
                            User user = new User (name, inputPass,inputEmail, age, weight);
                            db.AddUser(user);
                            sesssion.setLoggedIn(true);
                            sesssion.addPreferences("USER_NAME", user.getName());
                            sesssion.addPreferences("USER_EMAIL", user.getEmail());
                            sesssion.addPreferences("ID_USER", (String)jsonObj.get("id_user"));
                            Intent intent = new Intent(SingIn.this, UserActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                            finish();
                        }else{
                            dialog.dismiss();
                            emailInput.setText("");
                            passwordInput.setText("");
                            Toast.makeText(SingIn.this,"Zły login lub hasło spróbuj jeszcze raz !",
                                    Toast.LENGTH_LONG).show();
                        }
                    }else{
                        dialog.dismiss();
                        emailInput.setText("");
                        passwordInput.setText("");
                        Toast.makeText(SingIn.this, "Zły login lub hasło. Spróbuj jeszcze raz ",
                                Toast.LENGTH_SHORT).show();
                    }
                }catch(JSONException e){
                    e.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(SingIn.this,"Problem z uzyskaniem odpowiedzi od bazy danych",
                            Toast.LENGTH_SHORT);
                }
            }
        },new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("ERROR_JSON", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() {
                String email = emailInput.getText().toString();
                String password = passwordInput.getText().toString();
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };

        queue.add(strReq);
    }




}
