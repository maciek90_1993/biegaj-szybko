package com.example.maciej.biegajszybko;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.core.deps.guava.io.ByteArrayDataInput;
import android.support.test.espresso.core.deps.guava.io.ByteArrayDataOutput;
import android.support.test.espresso.core.deps.guava.io.Files;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.location.LocationListener;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.jar.*;

import static java.lang.String.valueOf;


public class UserActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SessionMenager sesssion;
    private ProgressDialog dialog;
    private TextView mTvTime;
    private TextView name_TV_headerNav1;
    private TextView email_TV_headerNav1;
    private TextView lap_time_content;
    private TextView speedValue;
    private Button btnStart;
    private Button btnStop;
    private ScrollView scrolView;
    private Button btnPause;
    private Chronometer chronometer;
    private Thread mThreadChronometer;
    private Context context;
    private float distance, timeTrening, hours, minuts, secunds, distanceInMeters,speed;
    private int tmp_distance;
    private TextView distanceValue;
    private BroadcastReceiver broadcastReceiver;
    private ArrayList<LatLng>points_track;
    private double lat, lng;
    private String tmpLat,tmpLng;
    private StatisticUser userStatistic;
    private PowerManager mgr;
    private PowerManager.WakeLock wakeLock;
    private DataBaseSQLLite db;
    private List<String>data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Trening");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        sesssion = new SessionMenager(getApplicationContext());
        dialog = new ProgressDialog(getApplicationContext());
        context = getApplicationContext();
        mTvTime = (TextView) findViewById(R.id.tv_time);
        name_TV_headerNav1 = (TextView) hView.findViewById(R.id.nameNav);
        email_TV_headerNav1 = (TextView) hView.findViewById(R.id.emailNav);
        btnStart = (Button) findViewById(R.id.btn_start);
        btnStop = (Button) findViewById(R.id.btn_stop);
        btnPause = (Button) findViewById(R.id.btn_pause);
        distanceValue = (TextView)findViewById(R.id.distance_value);
        speedValue = (TextView)findViewById(R.id.speedValue);
        speedValue.setText(String.format("%0$d km/h", 0));
        distanceValue.setText(String.format("%0$.1f km",0.0));
        lap_time_content = (TextView)findViewById(R.id.lap_time_content);
        scrolView = (ScrollView)findViewById(R.id.scrollView);
        tmp_distance =0;
        timeTrening = 0;
        name_TV_headerNav1.setText(sesssion.getPreference("USER_NAME"));
        email_TV_headerNav1.setText(sesssion.getPreference("USER_EMAIL"));
        points_track = new ArrayList<LatLng>();
        db = new DataBaseSQLLite(UserActivity.this);
        //Ustawienie onClick na przycisk start
        data = new ArrayList<String>();
        btnStart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                userStatistic = new StatisticUser();
                userStatistic.setDate(new Date());
                mgr = (PowerManager)getApplicationContext().getSystemService(Context.POWER_SERVICE);
                wakeLock= mgr.newWakeLock(PowerManager.FULL_WAKE_LOCK, "MyWakeLock");
                wakeLock.acquire();
                if(chronometer == null){
                    Intent i = new Intent(getApplicationContext(), GPS_Service.class);
                    startService(i);
                    chronometer = new Chronometer(UserActivity.this);
                    mThreadChronometer = new Thread(chronometer);
                    mThreadChronometer.start();
                    chronometer.start();

                }else {
                    if (chronometer != null) {

                        chronometer.start();
                    }
                }

            }
        });
        //Ustawienie onClick na przycisk stop
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chronometer != null){
                    chronometer.stop();
                    //The method interrupt() cause generate InterruptedException that we can see in android monitor.
                    mThreadChronometer.interrupt();

                    for (LatLng item : points_track) {
                        tmpLat = String.valueOf(item.latitude);
                        tmpLng = String.valueOf(item.longitude);
                        data.add(tmpLat+","+tmpLng+"-");
                    }
                    File file =  saveLocationPointToTXT(data);
                    try {
                        db.insertStatistic(mTvTime.getText().toString(),distanceValue.getText().toString(),new Date(), file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //resetTvTime();
                    mTvTime.setText("00:00:00:000");
                    mThreadChronometer = null;
                    chronometer = null;
                    userStatistic.setPoints_track(points_track);
                    distanceValue.setText(String.format("%0$.1f km",0.0));
                    lap_time_content.setText("");
                    speedValue.setText(String.format("%0$d km/h",0));
                    Intent i = new Intent(getApplicationContext(), GPS_Service.class);
                    stopService(i);
                    db.getAllStatisticUser();


                    new ExternalDatabaseStatisticUser(
                            UserActivity.this, sesssion.getPreference("ID_USER"),userStatistic).execute(ApiLoginConfig.URL_SAVE_STATISTIC);
                    points_track.clear();
                    try {
                        openTXTFileLocations();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //Ustawienie onCLick na przycisk pause
        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chronometer != null){
                    chronometer.pause();
                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            this.LogOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.userBox) {
            Intent intent = new Intent(UserActivity.this, YourAccount.class );
            startActivity(intent);

        } else if (id == R.id.tracks) {
            //track = new Intent(UserActivity.this, MapsActivity.class);
            Intent intent = new Intent(UserActivity.this, MapsActivity.class);

            startActivity(intent);

        } else if (id == R.id.statistic) {
            Intent intent = new Intent(UserActivity.this, Statistic.class);
            startActivity(intent);

        } else if(id == R.id.traning){
            Intent intent = new Intent(UserActivity.this, UserActivity.class);
            startActivity(intent);

        } else if(id == R.id.exit){
            AlertDialog.Builder builder = new AlertDialog.Builder(UserActivity.this);
            builder.setMessage(R.string.info_exit);
            builder.setCancelable(true);
            builder.setPositiveButton(R.string.info_exit_yes, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    UserActivity.this.finish();

                }
            });

            builder.setNegativeButton(R.string.info_exit_no, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void LogOut(){
        boolean login = sesssion.isLoggedIn();
        if(login){
            sesssion.setLoggedIn(false);
            sesssion.deleteAllPreferences();
            Intent intent = new Intent(getApplicationContext(), SingIn.class);
            startActivity(intent);
            finish();
            Toast.makeText(getApplicationContext(),"Zostałeś poprawnie wylogowany",Toast.LENGTH_SHORT).show();
        }else{
            finish();
            Toast.makeText(getApplicationContext(),"Problem z wylogowaniem",Toast.LENGTH_SHORT).show();
        }
    }

    public void updateTimerText(final String time){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mTvTime.setText(time);
                userStatistic.setTime(time);
                String[] tabTmp = time.split(":");
                hours = Float.valueOf(tabTmp[0]);
                minuts = Float.valueOf(tabTmp[1]);
                secunds = Float.valueOf(tabTmp[2]);
                timeTrening = 3600*hours+minuts*60+secunds;

            }
        });
    }



    public void updateDistanceText( final String distance){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                distanceValue.setText(distance);
            }
        });

    }
    public void updateDistance(float distance){
        distanceValue = (TextView)findViewById(R.id.distance_value);
        distanceValue.setText(valueOf(distance));
    }

    private boolean runtime_permissions(){
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},100);
            return true;
        }
        return false;

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    distanceValue.setText(String.format("%0$.1f km",intent.getExtras().get("Distance")));
                    userStatistic.setDistance(distanceValue.getText().toString());
                    distance = Float.valueOf(intent.getExtras().get("Distance").toString());
                    if(distance > 0){
                        distanceInMeters = Float.valueOf(intent.getExtras().get("DistanceInMeters").toString());
                        speed = (distanceInMeters/timeTrening)*(36/10);
                        speedValue.setText(String.format("%0$.0f km/h",speed ));
                        String tmp = String.valueOf(distance);
                        String[] tmp1 = tmp.split("\\.");
                        Log.d("TMP1: ",tmp1[0]);
                        if(tmp_distance < Integer.valueOf(tmp1[0])) {
                            tmp_distance = Integer.valueOf(tmp1[0]);
                            lap_time_content.append(tmp_distance + " km: " + String.valueOf(mTvTime.getText()) + "\n");

                            scrolView.post(new Runnable() {
                                @Override
                                public void run() {
                                    scrolView.smoothScrollTo(0, lap_time_content.getBottom());
                                }
                            });
                        }
                    }
                    String tmp2 = intent.getExtras().get("coordinates").toString();
                    String[] tmp3=tmp2.split("\\s");
                    lat = Double.valueOf(tmp3[1]);
                    lng = Double.valueOf(tmp3[0]);
                    points_track.add(new LatLng(lat,lng));
                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
        if(wakeLock !=null){
            wakeLock.release();

        }
    }

    private File saveLocationPointToTXT(List<String>data){

        File locationsFile = null;
        try {
            FileOutputStream file = openFileOutput("Locations.txt",MODE_PRIVATE);
            StringBuilder stringBuilder = new StringBuilder();

            for (String item : data) {
                stringBuilder.append(item);
            }
            file.write(stringBuilder.toString().getBytes());
            locationsFile = new File(getFilesDir(),"Locations.txt");
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return locationsFile;
    }

    private void openTXTFileLocations() throws IOException {
        FileInputStream file = openFileInput("Locations.txt");
        BufferedInputStream buffer = new BufferedInputStream(file);
        StringBuffer sb = new StringBuffer();
        while(buffer.available()!=0){
            char c = (char) buffer.read();
            sb.append(c);
        }

        Log.d("LOCATIONS.TXT",sb.toString());
    }
}
